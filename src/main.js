import { createApp } from 'vue';
import App from './App.vue';
import router from './router.js';
import store from './store/index.js';
import MainHeader from './components/layout/MainHeader.vue';
import BaseButton from './components/UI/BaseButton.vue';
import BasePost from './components/UI/BasePost.vue';
import BasePostDetail from './components/UI/BasePostDetail.vue';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faPaperPlane, faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

const app = createApp(App);

library.add(faPaperPlane, faTimes);

app.component('base-header', MainHeader);
app.component('base-button', BaseButton);
app.component('base-post', BasePost);
app.component('base-post-detail', BasePostDetail);
app.component('font-awesome-icon', FontAwesomeIcon);

app.use(router);
app.use(store);

app.mount('#app');
