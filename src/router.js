import { createRouter, createWebHistory } from 'vue-router';

import homepage from './pages/homepage/HomePage.vue';
import postItems from './pages/post-site/PostView.vue';
import makePost from './pages/add-post/MakePost.vue';
import postCategories from './pages/post-categories/PostCategories.vue';
import postCategory from './pages/post-categories/PostCategory.vue';
import notFound from './pages/NotFound.vue'

const router = createRouter({
    history: createWebHistory(),
    routes: [
        { path: '/', redirect: '/home' },
        { path: '/home', component: homepage },
        { path: '/posts/:id', component: postItems, props: true },
        { path: '/make-post', component: makePost },
        { path: '/post-categories', component: postCategories },
        { path: '/category/:id', component: postCategory, props: true },
        { path: '/:notFound(.*)', component: notFound }
    ]
});

export default router;