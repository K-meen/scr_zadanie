export default {
    registerPost(state, payload) {
        state.posts.push(payload);
    },
    registerCategory(state, payload){
        let allVals = [];
        if( state.images[Object.keys(payload)] ){
            const oldVals = Object.values( state.images[Object.keys(payload)] );
            oldVals.forEach( val => {
                allVals.push(val);
            })
        }
        
        let payloadVals = Object.values(payload);
        
        if(payloadVals[0][1] !== '') {
            allVals.push( payloadVals[0][0] );
            allVals.push( payloadVals[0][1] );
        }else {
            allVals.push( payloadVals[0][0] );
        }

        state.images[Object.keys(payload)] = allVals;
    }
}