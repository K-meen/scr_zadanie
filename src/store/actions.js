export default {
    addPost(context, data) {
        let id = context.getters.getPosts.length;

        const newPost = {
            id: id,
            category: data.category,
            title: data.title,
            citation: data.citation,
            images: data.images,
            text: data.text
        }

        let cat = newPost.category;
        let newCat = {};
        newCat[cat] = newPost.images;

        context.commit('registerPost', newPost);
        context.commit('registerCategory', newCat);
    }
}